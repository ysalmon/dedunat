let ascii = ref false
let is_ascii () = !ascii

let latex_exe = ref "lualatex"
let pdfview_exe = ref (if Sys.win32 then "explorer" else "xdg-open")
let (latex_preamble_file : string option ref) = ref None
let debug = ref false
